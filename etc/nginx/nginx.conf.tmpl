pid /var/run/nginx.pid;

user {{ .Env.SYSTEM_APPUSER_NAME }};
error_log {{ .Env.APP_BASEDIR_LOG }}/nginx_error.log notice;

worker_processes auto;
events {
  multi_accept on;
  use epoll;
  worker_connections 1024;
}

http {
    error_log {{ .Env.APP_BASEDIR_LOG }}/nginx_error.log notice;
    server {
        server_name   ~^(www\.)(?<domain>.+)$;
        expires       max;
        return        301 http://$domain$request_uri;
    }
    server {
        listen 80 default_server;
        root {{ .Env.APP_WEBROOT }};
        index index.php index.html;
        # Set docker subnets as trusted
        # set_real_ip_from 172.16.0.0/12;
        # set_real_ip_from 127.0.0.1;
        # Look for client IP in the X-Forwarded-For header
        real_ip_header X-Forwarded-For;
        # Ignore trusted IPs
        real_ip_recursive on;
        set $context {{ .Env.APP_ENV }};
        include     /etc/nginx/conf.d/*.conf;
    }

    # Root definitions
    include     /etc/nginx/nginx.d/*.conf;
    include     /etc/nginx/hosts.d/*.conf;

}
