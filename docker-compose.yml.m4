version: "3"
services:
  supporters:
    image: m4ReleaseImage():m4ReleaseImageTag()
    environment:
      - APP_ENV=dev
      - DB_USER
      - DB_HOST
      - DB_DATABASE
      - DB_PASSWORD
      - APP_BASEDIR_SRC=/app/src
      - APP_WEBROOT=/app/src/public

    networks:
      - internal
      - database
    ports:
      - 8081:80
    volumes:
    - ./app/src/:/app/src/:rw
    depends_on:
      - database
  database:
    image: mariadb:latest
    environment:
      MYSQL_DATABASE: $DB_DATABASE
      MYSQL_USER: $DB_USER
      MYSQL_PASSWORD: $DB_PASSWORD
      MYSQL_RANDOM_ROOT_PASSWORD: 1
    volumes:
      - db:/var/lib/mysql
    networks:
      - database
volumes:
  db: {}
  static: {}

networks:
  internal:
  database:
