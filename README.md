# Developers For Future - Website

This is the repository for the supporters app of [DevelopersForFuture](https://developersforfuture.org). We forked it from CCC's so called "engelsystem" which is an awesome base for such an app.

```
git clone git@github.com:developersforfuture/supporters.git
cd supporters
```

## Build and Deployment

tbd.

### Run the application

tbd.


#### 1 Use Docker

tbd.


### 3 Using Kubernetes

tbd.


## Development

tbd.
